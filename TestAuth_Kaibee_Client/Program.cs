﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace TestAuth_Kaibee_Client
{

    class Program
    {
        static  string apiBaseAddress;
        static readonly string DefaultEmail = "admin@admin.com";
        static readonly string DefaultPassword ="admin";

        static string Email;
        static string Password;

        static void Main(string[] args)
        {
            //recuperation de l'url du backend a partir du fichier App.config
            apiBaseAddress = System.Configuration.ConfigurationManager.AppSettings["apiBaseAddress"];
            char c =  '1';
            while (c != '0')
            {
                Console.WriteLine("1 : test api/Authenticate");
                Console.WriteLine("2 : test api/Confidentials");
                Console.WriteLine("0 : exit");

                c = Console.ReadKey().KeyChar;
                switch (c)
                {
                    case '1':
                         TestAuthentication().Wait();
                        break;

                    case '2':
                        TestConfidential().Wait();
                        break;

                    default: break;
                }

            }
        }

        /// <summary>
        /// Pemet le test du webservice api/Authenticate
        /// </summary>
        /// <returns></returns>
        static async Task TestAuthentication()
        {
            Console.WriteLine("\n test api/Authenticate");

            Console.WriteLine("saisissez votre email (par defaut : " + DefaultEmail + ")");
            Email = Console.ReadLine();
            if (string.IsNullOrEmpty(Email))
            {
                Email = DefaultEmail;
            }

            Console.WriteLine("saisissez votre mot de passe (par defaut : " + DefaultPassword + ")");
            Password = Console.ReadLine();
            if (string.IsNullOrEmpty(Password))
            {
                Password = DefaultPassword;
            }

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(apiBaseAddress + "api/Authenticate?email="+Email+"&password="+Password);
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
            }
            else
            {
                Console.WriteLine("Failed to call the API. HTTP Status: {0}, Reason {1}", response.StatusCode, response.ReasonPhrase);
            }
        }

        /// <summary>
        /// Pemet le test du webservice api/Confidentials
        /// </summary>
        /// <returns></returns>
        static async Task TestConfidential()
        {

            Console.WriteLine("\n test api/Confidentials");

           
            // Envoi d'une requete avec header d'authentification
            CustomDelegatingHandler customDelegatingHandler = new CustomDelegatingHandler(Email,Password);
            HttpClient client = HttpClientFactory.Create(customDelegatingHandler);


            HttpResponseMessage response = await client.GetAsync(apiBaseAddress + "api/Confidentials");

            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
            }
            else
            {
                Console.WriteLine("Failed to call the API. HTTP Status: {0}, Reason {1}", response.StatusCode, response.ReasonPhrase);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Permet l'ajout automatique du header d'auhentification
        /// </summary>
        public class CustomDelegatingHandler : DelegatingHandler
        {
            private string APPId ;
            private string APIKey ;


            public CustomDelegatingHandler(string email,string password)
            {
                this.APPId = email;
                this.APIKey = Base64Encode(password);
                Console.WriteLine("ID : " + APPId);
                Console.WriteLine("APIKey : " + APIKey);
            }

            protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {

                HttpResponseMessage response = null;
                string requestContentBase64String = string.Empty;

                string requestUri = System.Web.HttpUtility.UrlEncode(request.RequestUri.AbsoluteUri.ToLower());

                string requestHttpMethod = request.Method.Method;

                //Calculate UNIX time
                DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
                TimeSpan timeSpan = DateTime.UtcNow - epochStart;
                string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

                //create random nonce for each request
                string nonce = Guid.NewGuid().ToString("N");

                //Checking if the request contains body, usually will be null wiht HTTP GET and DELETE
                if (request.Content != null)
                {
                    byte[] content = await request.Content.ReadAsByteArrayAsync();
                    MD5 md5 = MD5.Create();
                    //Hashing the request body, any change in request body will result in different hash, we'll incure message integrity
                    byte[] requestContentHash = md5.ComputeHash(content);
                    requestContentBase64String = Convert.ToBase64String(requestContentHash);
                }

                //Creating the raw signature string
                string signatureRawData = String.Format("{0}{1}{2}{3}{4}{5}", APPId, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);

                var secretKeyByteArray = Convert.FromBase64String(APIKey);

                byte[] signature = Encoding.UTF8.GetBytes(signatureRawData);

                using (HMACSHA256 hmac = new HMACSHA256(secretKeyByteArray))
                {
                    byte[] signatureBytes = hmac.ComputeHash(signature);
                    string requestSignatureBase64String = Convert.ToBase64String(signatureBytes);
                    //Setting the values in the Authorization header using custom scheme (amx)
                    request.Headers.Authorization = new AuthenticationHeaderValue("amx", string.Format("{0}:{1}:{2}:{3}", APPId, requestSignatureBase64String, nonce, requestTimeStamp));
                }

                response = await base.SendAsync(request, cancellationToken);

                return response;
            }
        }

        private void GenerateAPPKey()
        {
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                byte[] secretKeyByteArray = new byte[32]; //256 bit
                cryptoProvider.GetBytes(secretKeyByteArray);
                var APIKey = Convert.ToBase64String(secretKeyByteArray);
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }

}
