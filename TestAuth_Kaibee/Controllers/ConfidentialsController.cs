﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAuth_Kaibee.services;

namespace TestAuth_Kaibee.Controllers
{
    public class ConfidentialsController : ApiController
    {
       
        public async System.Threading.Tasks.Task<bool> GetAsync()
        {
            HMACAuthentication hMACAuthentication = new HMACAuthentication();
            //verification l'authentification
            bool isAuthenticated = await hMACAuthentication.AuthenticateAsync(Request);
            if (isAuthenticated)
                return true;
            else
                return false;
        }
    }
}
